<?php
/*
 *  Copyright (C) 2022  Anedroid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);
declare(ticks=1);

class Main
{
    const COMMANDS = ['usage', 'server', 'client'];
    const DEFAULT_SOCKET = 'server.sock';
    const PING_TIME = 1000000;
    const PING_COUNT = 4;
    
    public $socket;
    public array $peers = [];
    
    public function __construct(string $command, ...$args)
    {
        if (self::$initialized) {
            fwrite(STDERR, "Already initialized\n");
            return;
        }
        self::$initialized = true;
        if (in_array($command, self::COMMANDS)) {
            $this->$command(...$args);
        }
    }
    
    public function usage(): void
    {
        global $argv;
        echo
"usage:
    $argv[0] server [socket]
    $argv[0] client [socket]\n";
    }
    
    public function server(string $filename): void
    {   
        $this->socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
        $this::$resources[] = $this->socket;
        
        self::$tmpfiles[] = $filename;
        if (!@socket_bind($this->socket, $filename)) {
            $errno = socket_last_error($this->socket);
            $errmsg = socket_strerror($errno);
            fwrite(STDERR, "Error $errno: $errmsg\n");
            return;
        }
        
        pcntl_signal(SIGINT, [self::class, 'server_shutdown']);
        register_shutdown_function([self::class, 'server_shutdown']);
        
        if (!@socket_listen($this->socket)) {
            $errno = socket_last_error($this->socket);
            $errmsg = socket_strerror($errno);
            fwrite(STDERR, "Error $errno: $errmsg\n");
            return;
        }
        echo "Listening on $filename\n";
        
        socket_set_nonblock($this->socket);
        
        while (true) {
            $connection = socket_accept($this->socket);
            if ($connection === false) {
                foreach ($this->peers as $i => &$peer) {
                    if (hrtime(true) >= $peer['time'] + self::PING_TIME * 1000) {
                        if(!@$this->ping($peer)) {
                            $this->disconnect($i);
                            continue;
                        }
                        $peer['time'] = hrtime(true);
                        
                        if ($peer['counter'] > self::PING_COUNT) {
                            $this->disconnect($i);
                        }
                    }
                }
                
                usleep(100000);
                continue;
            }
            
            self::$resources[] = $connection;
            $id = count($this->peers) + 1;
            $this->peers[] = [
                'socket' => $connection,
                'time' => hrtime(true) - self::PING_TIME * 1000,
                'counter' => 1,
                'id' => $id
            ];
            
            $time = date('H:i:s');
            echo "$time peer $id connected\n";
            socket_write($connection, "You are peer $id\n");
        }
    }
    
    public function client(string $filename): void
    {
        if (count(glob($filename)) === 0) {
            fwrite(STDERR, "Server offline\n");
            return;
        }
        
        $this->socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
        $this::$resources[] = $this->socket;
        
        socket_connect($this->socket, $filename);
        
        while ($data = socket_read($this->socket, 1024)) {
            echo $data;
        }
    }
    
    private function ping(array &$peer)
    {
        $time = date('H:i:s');
        echo 'ping ' . $peer['counter'] . '/' . self::PING_COUNT . ' to peer ' . $peer['id'] . "\n";
        $peer['counter']++;
        return socket_write($peer['socket'], "Current time: $time\n");
    }
    
    private function disconnect(int $index)
    {
        $peer = $this->peers[$index];
        if (is_resource($peer['socket'])) {
            socket_close($peer['socket']);
        }
        $time = date('H:i:s');
        $id = $peer['id'];
        echo "$time peer $id disconnected\n";
        unset($this->peers[$index]);
    }
    
    public static function init(int $argc, array $argv): void
    {
        define('WORKING_DIRECTORY', getcwd());
        chdir(__DIR__);
        
        if ($argc <= 1) {
            new self('usage');
            return;
        }
        
        switch ($argv[1]) {
        case 'server':
            new self('server', $argv[2] ?? self::DEFAULT_SOCKET);
            break;
        case 'client':
            new self('client', $argv[2] ?? self::DEFAULT_SOCKET);
            break;
        default:
            new self('usage');
            break;
        }
    }
    
    public static function server_shutdown(): void
    {
        if (self::$closed) {
            return;
        }
        echo "shutting down\n";
        
        $counter = 0;
        foreach (self::$resources as $resource) {
            if (is_resource($resource)) {
                socket_close($resource);
                $counter++;
            }
        }
        if ($counter > 0) {
            echo "closed $counter resources\n";
        }
        
        $counter = 0;
        foreach (self::$tmpfiles as $file) {
            unlink($file);
            $counter++;
        }
        if ($counter > 0) {
            echo "deleted $counter temporary files\n";
        }
        
        self::$closed = true;
        exit;
    }
    
    private static bool $initialized = false;
    private static bool $closed = false;
    private static array $resources = [];
    private static array $tmpfiles = [];
}

Main::init($argc, $argv);
