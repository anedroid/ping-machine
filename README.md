# ping machine

Example PHP application using sockets. Server listens for connections on choosen port (inet variant) or socket file (unix variant) and when the client connects, it sends 4 "pings" (lines with current time) and then closes the connection. It can handle multiple peers at the same time thanks to non-blocking sockets. The client just prints everything on the console.

Inet variant automatically try to bind next port if the current one is not available.

## Usage – inet variant

This variant use network TCP ports.

```bash
php ping-machine-inet.php server [addr] [port]
php ping-machine-inet.php client [addr] [port]
```

The default address is localhost, and the default port is 1234.

## Usage – unix variant

This variant use unix sockets.

```bash
php ping-machine-unix.php server [socket]
php ping-machine-unix.php client [socket]
```

The default socket filename is `server.sock`.

## License

Every piece of software I ever develop will be Free Software.
Even though it's a very simple program, it should also be licenced under
Free license to let others learn from its source code, and create something fun.

Copyright (C) 2022  Anedroid

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a [copy of the GNU General Public License](LICENSE)
along with this program.  If not, see <https://www.gnu.org/licenses/>.

GNU GPL version 3 is a copyleft license, that means you have all 4 freedoms to
run, study, modify and share the software, but you can't deny these freedoms
from others. Don't make non-free software – it hurts the community!
